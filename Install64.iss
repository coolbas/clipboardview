#define VersionString "1.0.6.9"
#define AppName "ClipboardView"
#define MenuGroup "ClipboardView x64"
#define AppPublisher "Sergiy Kovbasa"
#define AppExeName "ClipboardView.exe"


[Setup]
AppId={{A1235516-A57D-430E-B92B-4CAF9630EC69}
AppName={#MenuGroup}
AppVersion={#VersionString}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#AppPublisher}
DefaultDirName={pf}\{#AppName}
DefaultGroupName={#MenuGroup}
OutputBaseFilename=InstallClipboardView64
Compression=lzma
SolidCompression=yes
ArchitecturesAllowed=x64 ia64
ArchitecturesInstallIn64BitMode=x64

[UninstallDelete]
Type: files; Name: "{app}\ClipboardView.ini"
Type: files; Name: "{app}\History.dat" 

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"
Name: "ukrainian"; MessagesFile: "compiler:Languages\Ukrainian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "D:\Delphi19\Out\Bin64\ClipboardView.exe"; DestDir: "{app}"; DestName: "ClipboardView.exe"; Check: Is64BitInstallMode

[Icons]
Name: "{group}\{#MenuGroup}"; Filename: "{app}\{#AppExeName}"
Name: "{group}\{cm:UninstallProgram,{#AppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MenuGroup}"; Filename: "{app}\{#AppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#AppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(AppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

