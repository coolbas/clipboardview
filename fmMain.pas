unit fmMain;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, System.AnsiStrings,
  System.Generics.Collections, System.IniFiles,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Clipbrd,
  Vcl.Menus, Vcl.ExtCtrls, Vcl.ComCtrls, System.Actions, Vcl.ActnList,
  Vcl.StdActns, Vcl.Samples.Spin,
  fmPopup, uConsts;

type
  TExList = class(TList<String>)
  strict private
    procedure LoadFromStream(AStream: TStream);
    procedure SaveToStream(AStream: TStream);
  public
    procedure LoadFromZip(const AFileName: String);
    procedure LoadFromFile(const AFileName: String);
    procedure SaveToZip(const AFileName: String);
    procedure SaveToFile(const AFileName: String);
  end;

  TGlobalHotkey = class(TObject)
  strict private
    FHandle: HWND;
    FHotKeyID: Word;
    FKey: Cardinal;
    FModifiers: Cardinal;
    FOnHotKey: TNotifyEvent;
  private
    procedure WndProc(var AMsg: TMessage);
  public
    constructor Create(AKey, AModifiers: Cardinal; AOnHotKey: TNotifyEvent);
    destructor Destroy; override;
    procedure RegisterHotKey;
    procedure UnRegisterHotKey;
  end;

  TfrmMain = class(TForm)
    bExit: TButton;
    tyMain: TTrayIcon;
    pmTray: TPopupMenu;
    lbHistory: TListBox;
    ActionList: TActionList;
    acClear: TAction;
    bClear: TButton;
    seHistCount: TSpinEdit;
    seTrayCount: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    acExit: TAction;
    Label3: TLabel;
    acRestore: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acClearExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure tyMainClick(Sender: TObject);
    procedure acRestoreExecute(Sender: TObject);
    procedure seHistCountChange(Sender: TObject);
    procedure seTrayCountChange(Sender: TObject);
    procedure lbHistoryKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure lbHistoryMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer);
    procedure FormResize(Sender: TObject);
  private
    FClipboardHistory: TExList;
    FClipboardViewer: HWND;
    FIsOnHotKey: Boolean;
    FFullHistCount: Integer;
    FHotKey: TGlobalHotkey;
    FLastClipbrdText: String;
    FPrevItemHotKey: TGlobalHotkey;
    FTrayHistCount: Integer;
    procedure ClearHistory;
    procedure ClipboardChanged;
    procedure CMVisibleChanged(var AMessage: TMessage); message CM_VISIBLECHANGED;
    procedure CreateTrayMenu;
    procedure DoOnHotKey(ASender: TObject);
    procedure DoOnPrevItemHotKey(ASender: TObject);
    function GetShortItemText(const AText: String; AMaxWidth: Integer): String;
    procedure LoadSettings;
    procedure LoadToListView;
    procedure MenuItemClick(ASender: TObject);
    procedure DoOnMinimize(ASender: TObject);
    procedure RestoreForm;
    procedure RestoreWindow;
    procedure SetFullHistCount(const AValue: Integer);
    procedure SetClipBoardFromList(AIndex: Integer);
    procedure SetTrayHistCount(const AValue: Integer);
    procedure StoreForm;
  protected
    procedure WndProc(var AMsg: TMessage); override;
  public
    property FullHistCount: Integer read FFullHistCount write SetFullHistCount;
    property TrayHistCount: Integer read FTrayHistCount write SetTrayHistCount;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  System.Zip, System.Math;

{$R *.dfm}

function StringToHandle(const AStr: String): THandle; overload;
var
  pData: Pointer;
  Len: Cardinal;
begin
  Len := Succ(Length(AStr)) * SizeOf(Char);
  Result := GlobalAlloc(GMEM_MOVEABLE or GMEM_DDESHARE or GMEM_ZEROINIT, Len);
  try
    pData := GlobalLock(Result);
    try
      Move(PChar(AStr)^, pData^, Len);
    finally
      GlobalUnlock(Result);
    end;
  except
    GlobalFree(Result);
    raise
  end;
end;

function StringToHandle(const AStr: AnsiString; AIsUnicode: Boolean): THandle; overload;
var
  pData: Pointer;
  Len, Res, Size: Cardinal;
begin
  Len := Length(AStr);
  if AIsUnicode then
    Size := Succ(MultiByteToWideChar(0, 0, PAnsiChar(AStr), Len, nil, 0)) * SizeOf(WideChar)
  else
    Size := Succ(Len);
  Result := GlobalAlloc(GMEM_MOVEABLE or GMEM_ZEROINIT or GMEM_DDESHARE, Size);
  try
    pData := GlobalLock(Result);
    try
      if AIsUnicode then begin
        Res := MultiByteToWideChar(0, 0, PAnsiChar(AStr), Len, pData, Size);
        if Res <> NO_ERROR then
          raise Exception.Create(SysErrorMessage(Res));
      end else
        System.AnsiStrings.StrCopy(pData, PAnsiChar(AStr));
    finally
      GlobalUnlock(Result);
    end;
  except
    GlobalFree(Result);
    raise;
  end;
end;

function GetStringFromHandle(AHandle: THandle; AIsUnicode: Boolean): String;
var
  pData: Pointer;
begin
  Result := EmptyStr;
  if AHandle <> 0 then
    try
      pData := GlobalLock(AHandle);
      if AIsUnicode then begin
        Result := PChar(pData);
      end else
        Result := String(PAnsiChar(pData));
    finally
      GlobalUnlock(AHandle);
    end;
end;

procedure TfrmMain.acClearExecute(Sender: TObject);
begin
  FClipboardHistory.Clear;
  ClipboardChanged;
end;

procedure TfrmMain.acExitExecute(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmMain.acRestoreExecute(Sender: TObject);
begin
  RestoreWindow;
end;

procedure TfrmMain.ClipboardChanged;
var
  Format: Word;
  TextFromClibrd: String;
begin
  if Clipboard.HasFormat(CF_UNICODETEXT) then
    Format := CF_UNICODETEXT
  else if Clipboard.HasFormat(CF_TEXT) then
    Format := CF_TEXT
  else
    Format := 0;
  if Format > 0 then begin
    TextFromClibrd := GetStringFromHandle(Clipboard.GetAsHandle(Format), Clipboard.HasFormat(CF_UNICODETEXT));
    if not SameStr(TextFromClibrd, FLastClipbrdText) then begin
      FLastClipbrdText := TextFromClibrd;
      FClipboardHistory.Insert(0, FLastClipbrdText);
    end;
  end;
  ClearHistory;
  LoadToListView;
  CreateTrayMenu;
  FClipboardHistory.SaveToFile(ExtractFilePath(ParamStr(0)) + SFileName);
end;

procedure TfrmMain.CMVisibleChanged(var AMessage: TMessage);
begin
  if HandleAllocated then
    if Visible then
      RestoreForm
    else
      StoreForm;
  inherited;
  if WindowState = wsMinimized then
    DoOnMinimize(Self);
end;

procedure TfrmMain.CreateTrayMenu;
var
  I, MaxIndex: Integer;
  Items: array of TMenuItem;
begin
  pmTray.Items.Clear;
  if Pred(FClipboardHistory.Count) < TrayHistCount then
    MaxIndex := Pred(FClipboardHistory.Count)
  else
    MaxIndex := TrayHistCount;
  for I := 0 to MaxIndex do begin
    SetLength(Items, Succ(Length(Items)));
    Items[I] := pmTray.CreateMenuItem;
    Items[I].OnClick := MenuItemClick;
    Items[I].Tag := I;
    Items[I].Caption := GetShortItemText(FClipboardHistory[I], ShortTxtWidthForPopupMenu);
  end;
  I := Length(Items);
  SetLength(Items, I + 3);
  Items[I] := pmTray.CreateMenuItem;
  Items[I].Caption := '-';
  Items[I + 1] := pmTray.CreateMenuItem;
  Items[I + 1].Default := True;
  Items[I + 1].Action := acRestore;
  Items[I + 2] := pmTray.CreateMenuItem;
  Items[I + 2].Action := acExit;
  pmTray.Items.Add(Items);
end;

procedure TfrmMain.DoOnHotKey(ASender: TObject);
begin
  if not FIsOnHotKey then begin
    FIsOnHotKey := True;
    try
      if FClipboardHistory.Count > 0 then
        if IsIconic(Handle) then
          SetClipBoardFromList(TfrmPopup.Execute(lbHistory.Items))
        else
          RestoreWindow
      else
        MessageBox(Self.Handle, PWideChar(SNoElements), PWideChar(SInfo), MB_ICONINFORMATION or
          MB_OK or MB_TOPMOST or MB_TASKMODAL);
    finally
      FIsOnHotKey := False;
    end;
  end else
    Application.BringToFront;
end;

procedure TfrmMain.DoOnMinimize(ASender: TObject);
begin
  ShowWindow(frmMain.Handle, SW_HIDE);
end;

procedure TfrmMain.DoOnPrevItemHotKey(ASender: TObject);
begin
  if FClipboardHistory.Count > 1 then
    SetClipBoardFromList(1);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  FileName: String;
begin
  LoadSettings;
  FClipboardHistory := TExList.Create;
  FileName := ExtractFilePath(ParamStr(0)) + SFileName;
  if FileExists(FileName) then
    try
      FClipboardHistory.LoadFromFile(FileName);
    except
      DeleteFile(FileName);
      raise Exception.Create(SFileCorruptWillDelete);
    end;
  ClearHistory;
  LoadToListView;
  CreateTrayMenu;
  Application.OnMinimize := DoOnMinimize;
  FHotKey := TGlobalHotkey.Create(VK_OEM_3, MOD_WIN, DoOnHotKey);
  FPrevItemHotKey := TGlobalHotkey.Create(VK_V, MOD_CONTROL or MOD_SHIFT, DoOnPrevItemHotKey);
  FClipboardViewer := SetClipboardViewer(Handle);
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  _SettingsFile.UpdateFile;
  ChangeClipboardChain(Handle, FClipboardViewer);
  FreeAndNil(FPrevItemHotKey);
  FreeAndNil(FHotKey);
  FreeAndNil(FClipboardHistory);
end;

procedure TfrmMain.FormResize(Sender: TObject);
begin
  LoadToListView;
end;

function TfrmMain.GetShortItemText(const AText: String; AMaxWidth: Integer): String;
var
  i: Integer;
  LText: String;
begin
  LText := AText.Replace(#13, #32).Replace(#10, #32);
  Result := LText;
  i := 1;
  while (i <= Length(LText)) and (Canvas.TextWidth(Result) > AMaxWidth) do begin
    Inc(i);
    Result := Copy(LText, 1, Max(0, Length(LText) - i)) + '...';
  end;
end;

procedure TfrmMain.lbHistoryKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    SetClipBoardFromList(lbHistory.ItemIndex);
end;

procedure TfrmMain.lbHistoryMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  SetClipBoardFromList(lbHistory.ItemIndex);
end;

procedure TfrmMain.LoadSettings;
begin
  if Assigned(_SettingsFile) then begin
    FullHistCount := _SettingsFile.ReadInteger(SConstraintsSect, SFullHistCountOpt,
      FullHistCountDef);
    TrayHistCount := _SettingsFile.ReadInteger(SConstraintsSect, STrayHistCountOpt,
      TrayHistCountDef);
  end;
end;

procedure TfrmMain.LoadToListView;
var
  I: Integer;
begin
  lbHistory.Items.BeginUpdate;
  try
    lbHistory.Clear;
    for I := 0 to Pred(FClipboardHistory.Count) do
      lbHistory.Items.Insert(I, GetShortItemText(FClipboardHistory[I], lbHistory.ClientWidth));
  finally
    lbHistory.Items.EndUpdate;
  end;
end;

procedure TfrmMain.MenuItemClick(ASender: TObject);
begin
  if ASender is TMenuItem then
    SetClipBoardFromList(TMenuItem(ASender).Tag);
end;

procedure TfrmMain.WndProc(var AMsg: TMessage);
begin
  case AMsg.Msg of
    WM_DRAWCLIPBOARD: begin
      ClipboardChanged;
      SendMessage(FClipboardViewer, AMsg.Msg, AMsg.WParam, AMsg.LParam);
    end;
    WM_CHANGECBCHAIN: begin
      if AMsg.WParam = FClipboardViewer then
        FClipboardViewer := AMsg.LParam
      else
        SendMessage(FClipboardViewer, WM_CHANGECBCHAIN, AMsg.WParam, AMsg.LParam);
      AMsg.Result := 0;
    end;
    else
      inherited WndProc(AMsg);
  end;
end;

procedure TfrmMain.RestoreForm;
begin
  if Assigned(_SettingsFile) then begin
    WindowState := TWindowState(_SettingsFile.ReadInteger(SMainWindowSect, SWindowState, 0));
    if WindowState <> wsMaximized then begin
      Left := _SettingsFile.ReadInteger(SMainWindowSect, SLeft, 0);
      Top := _SettingsFile.ReadInteger(SMainWindowSect, STop, 0);
      Height := _SettingsFile.ReadInteger(SMainWindowSect, SHeight, Constraints.MinHeight);
      Width := _SettingsFile.ReadInteger(SMainWindowSect, SWidth, Constraints.MinWidth);
    end;
  end;
end;

procedure TfrmMain.RestoreWindow;
begin
  if (IsIconic(Handle) or (Self.WindowState = wsMinimized)) and not FIsOnHotKey then
    ShowWindow(frmMain.Handle, SW_RESTORE);
  Application.BringToFront;
end;

procedure TfrmMain.seHistCountChange(Sender: TObject);
begin
  FullHistCount := seHistCount.Value;
end;

procedure TfrmMain.SetClipBoardFromList(AIndex: Integer);
const
  MaxRetryCount: Integer = 5;
var
  IsSuccess: Boolean;
  RetryCount: Integer;
begin
  IsSuccess := False;
  RetryCount := 0;
  while not IsSuccess do
    try
      if AIndex > -1 then begin
        Clipboard.Open;
        try
          Clipboard.Clear;
          Clipboard.SetAsHandle(CF_UNICODETEXT, StringToHandle(FClipboardHistory[AIndex]));
          Clipboard.SetAsHandle(CF_TEXT, StringToHandle(AnsiString(FClipboardHistory[AIndex]), False));
        finally
          Clipboard.Close;
        end;
      end;
      IsSuccess := True;
    except
      on Exception do begin
        Inc(RetryCount);
        if RetryCount < MaxRetryCount then
          Sleep(200)
        else
          raise;
      end;
    end;
end;

procedure TfrmMain.ClearHistory;
var
  I: Integer;
begin
  if FClipboardHistory.Count > FullHistCount then
    for I := Pred(FClipboardHistory.Count) downto FullHistCount do
      FClipboardHistory.Delete(I);
end;

procedure TfrmMain.SetFullHistCount(const AValue: Integer);
begin
  if FFullHistCount <> AValue then begin
    FFullHistCount := AValue;
    seHistCount.Value := AValue;
    _SettingsFile.WriteInteger(SConstraintsSect, SFullHistCountOpt, AValue);
  end;
end;

procedure TfrmMain.seTrayCountChange(Sender: TObject);
begin
  TrayHistCount := seTrayCount.Value;
end;

procedure TfrmMain.SetTrayHistCount(const AValue: Integer);
begin
  if FTrayHistCount <> AValue then begin
    if AValue > FFullHistCount then
      raise Exception.CreateFmt(STrayCntLargerFull, [AValue, FFullHistCount]);
    FTrayHistCount := AValue;
    seTrayCount.Value := AValue;
    _SettingsFile.WriteInteger(SConstraintsSect, STrayHistCountOpt, AValue);
  end;
end;

procedure TfrmMain.StoreForm;
begin
  if Assigned(_SettingsFile) then begin
    if WindowState <> wsMaximized then begin
      _SettingsFile.WriteInteger(SMainWindowSect, SLeft, Left);
      _SettingsFile.WriteInteger(SMainWindowSect, STop, Top);
      _SettingsFile.WriteInteger(SMainWindowSect, SHeight, Height);
      _SettingsFile.WriteInteger(SMainWindowSect, SWidth, Width);
    end;
    _SettingsFile.WriteInteger(SMainWindowSect, SWindowState, Integer(WindowState));
  end;
end;

procedure TfrmMain.tyMainClick(Sender: TObject);
begin
  RestoreWindow;
end;

{ TExList }

procedure TExList.LoadFromFile(const AFileName: String);
var
  Stream: TStream;
begin
  Stream := TFileStream.Create(AFileName, fmOpenRead);
  try
    LoadFromStream(Stream);
  finally
    FreeAndNil(Stream);
  end;
end;

procedure TExList.LoadFromStream(AStream: TStream);
var
  Item: String;
  Reader: TReader;
begin
  Clear;
  Reader := TReader.Create(AStream, 1024);
  try
    Reader.ReadListBegin;
    while not Reader.EndOfList do begin
      Item := Reader.ReadString;
      Add(Item);
    end;
    Reader.ReadListEnd;
  finally
    FreeAndNil(Reader);
  end;
end;

procedure TExList.LoadFromZip(const AFileName: String);
var
  Stream: TStream;
  LocalHeader: TZipHeader;
  ZipFile: TZipFile;
begin
  ZipFile := TZipFile.Create;
  try
    ZipFile.Open(AFileName, zmRead);
    try
      ZipFile.Read(ChangeFileExt(ExtractFileName(AFileName), SDatExt), Stream, LocalHeader);
      try
        LoadFromStream(Stream);
      finally
        FreeAndNil(Stream);
      end;
    finally
      ZipFile.Close;
    end;
  finally
    FreeAndNil(ZipFile);
  end;
end;

procedure TExList.SaveToFile(const AFileName: String);
var
  Stream: TStream;
begin
  Stream := TFileStream.Create(AFileName, fmCreate);
  try
    SaveToStream(Stream);
  finally
    FreeAndNil(Stream);
  end;
end;

procedure TExList.SaveToStream(AStream: TStream);
var
  Writer: TWriter;
  Item: String;
begin
  Writer := TWriter.Create(AStream, 4096);
  try
    Writer.WriteListBegin;
    for Item in Self do begin
      Writer.WriteString(Item);
    end;
    Writer.WriteListEnd;
  finally
    FreeAndNil(Writer);
  end;
end;

procedure TExList.SaveToZip(const AFileName: String);
var
  Stream: TStream;
  ZipFile: TZipFile;
begin
  Stream := TMemoryStream.Create;
  try
    SaveToStream(Stream);
    Stream.Position := 0;
    ZipFile := TZipFile.Create;
    try
      ZipFile.Open(AFileName, zmWrite);
      ZipFile.Add(Stream, ChangeFileExt(ExtractFileName(AFileName), SDatExt));
      ZipFile.Close;
    finally
      FreeAndNil(ZipFile);
    end;
  finally
    FreeAndNil(Stream);
  end;
end;

{ TGlobalHotkey }

constructor TGlobalHotkey.Create(AKey, AModifiers: Cardinal; AOnHotKey: TNotifyEvent);
begin
  FHandle := AllocateHWnd(WndProc);
  FHotKeyID := GlobalAddAtom(PChar(SGlobalHotKey));
  FModifiers := AModifiers;
  FKey := AKey;
  FOnHotKey := AOnHotKey;
  RegisterHotKey;
end;

destructor TGlobalHotkey.Destroy;
begin
  UnRegisterHotKey;
  GlobalDeleteAtom(FHotKeyID);
  DeallocateHWnd(FHandle);
  inherited Destroy;
end;

procedure TGlobalHotkey.RegisterHotKey;
begin
  Winapi.Windows.RegisterHotKey(FHandle, FHotKeyID, FModifiers, FKey);
end;

procedure TGlobalHotkey.UnRegisterHotKey;
begin
  Winapi.Windows.UnRegisterHotKey(FHandle, FHandle);
end;

procedure TGlobalHotkey.WndProc(var AMsg: TMessage);
begin
  if (AMsg.Msg = WM_HOTKEY) and Assigned(FOnHotKey) then
    FOnHotKey(Self)
  else
    AMsg.Result := DefWindowProc(FHandle, AMsg.Msg, AMsg.WParam, AMsg.LParam);
end;

end.
