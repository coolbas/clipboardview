unit fmPopup;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, System.IniFiles,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  uConsts;

type
  TfrmPopup = class(TForm)
    lbHistory: TListBox;
    procedure FormShow(Sender: TObject);
    procedure lbHistoryKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbHistoryMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
  private
    procedure CMVisibleChanged(var AMessage: TMessage); message CM_VISIBLECHANGED;
    procedure RestoreForm;
    procedure StoreForm;
  public
    class function Execute(const AList: TStrings): Integer;
  end;

var
  frmPopup: TfrmPopup;

implementation

{$R *.dfm}

{ TfrmPopup }

procedure TfrmPopup.CMVisibleChanged(var AMessage: TMessage);
begin
  if HandleAllocated then
    if Visible then
      RestoreForm
    else
      StoreForm;
  inherited;
end;

class function TfrmPopup.Execute(const AList: TStrings): Integer;
var
  Form: TfrmPopup;
begin
  Result := -1;
  Form := TfrmPopup.Create(nil);
  try
    Form.lbHistory.Clear;
    Form.lbHistory.Items.Assign(AList);
    if Form.ShowModal = mrOk then
      Result := Form.lbHistory.ItemIndex;
  finally
    FreeAndNil(Form);
  end;
end;

procedure TfrmPopup.FormDestroy(Sender: TObject);
begin
  _SettingsFile.UpdateFile;
end;

procedure TfrmPopup.FormShow(Sender: TObject);
begin
  SetForegroundWindow(Handle);
end;

procedure TfrmPopup.lbHistoryKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    Self.ModalResult := mrOk;
end;

procedure TfrmPopup.lbHistoryMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Self.ModalResult := mrOk;
end;

procedure TfrmPopup.RestoreForm;
begin
  if Assigned(_SettingsFile) then begin
    Left := _SettingsFile.ReadInteger(SPopupWindowSect, SLeft, 0);
    Top := _SettingsFile.ReadInteger(SPopupWindowSect, STop, 0);
  end;
end;

procedure TfrmPopup.StoreForm;
begin
  if Assigned(_SettingsFile) then begin
    _SettingsFile.WriteInteger(SPopupWindowSect, SLeft, Left);
    _SettingsFile.WriteInteger(SPopupWindowSect, STop, Top);
  end;
end;

end.
