object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'ClipboardView'
  ClientHeight = 292
  ClientWidth = 398
  Color = clBtnFace
  Constraints.MinHeight = 330
  Constraints.MinWidth = 414
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  DesignSize = (
    398
    292)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 243
    Width = 145
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = '���������� ����� � �������'
    ExplicitTop = 285
  end
  object Label2: TLabel
    Left = 6
    Top = 267
    Width = 128
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = '���������� ����� � ����'
    ExplicitTop = 309
  end
  object Label3: TLabel
    Left = 8
    Top = 219
    Width = 311
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = '����� ������������ ���� ������� Win + � (Win + `)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ExplicitTop = 261
  end
  object bExit: TButton
    Left = 315
    Top = 261
    Width = 75
    Height = 25
    Action = acExit
    Anchors = [akRight, akBottom]
    TabOrder = 4
  end
  object lbHistory: TListBox
    Left = 6
    Top = 6
    Width = 384
    Height = 208
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnKeyUp = lbHistoryKeyUp
    OnMouseUp = lbHistoryMouseUp
  end
  object bClear: TButton
    Left = 315
    Top = 233
    Width = 75
    Height = 25
    Action = acClear
    Anchors = [akRight, akBottom]
    TabOrder = 3
  end
  object seHistCount: TSpinEdit
    Left = 184
    Top = 238
    Width = 49
    Height = 22
    Anchors = [akLeft, akBottom]
    MaxValue = 9999999
    MinValue = 0
    TabOrder = 1
    Value = 0
    OnChange = seHistCountChange
  end
  object seTrayCount: TSpinEdit
    Left = 184
    Top = 264
    Width = 49
    Height = 22
    Anchors = [akLeft, akBottom]
    MaxValue = 9999999
    MinValue = 0
    TabOrder = 2
    Value = 0
    OnChange = seTrayCountChange
  end
  object tyMain: TTrayIcon
    PopupMenu = pmTray
    Visible = True
    OnClick = tyMainClick
    Left = 200
    Top = 98
  end
  object pmTray: TPopupMenu
    Left = 272
    Top = 96
  end
  object ActionList: TActionList
    Left = 128
    Top = 96
    object acClear: TAction
      Category = 'Window'
      Caption = '��������'
      OnExecute = acClearExecute
    end
    object acExit: TAction
      Category = 'Window'
      Caption = '�����'
      OnExecute = acExitExecute
    end
    object acRestore: TAction
      Category = 'Window'
      Caption = '���������'
      OnExecute = acRestoreExecute
    end
  end
end
