program ClipBoardView;

{$R 'VersionInfo.res' 'VersionInfo.rc'}

uses
  Vcl.Forms,
  fmMain in 'fmMain.pas' {frmMain},
  fmPopup in 'fmPopup.pas' {frmPopup},
  uConsts in 'uConsts.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'ClipboardView';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
