unit uConsts;

interface

uses
  System.SysUtils, System.IniFiles;

resourcestring
  STrayCntLargerFull = '���������� ����� � ���� (%d) �� ������ ��������� ����� ���������� ����� � ������� (%d)';
  SFileCorruptWillDelete = '�������� ���� ������� ������, �������� �� ���������';
  SInfo = '����������';
  SNoElements = '������� � ����� ������ ����� ��� �� �������� �����';

const
  FullHistCountDef = 20;
  TrayHistCountDef = 10;
  ShortTxtWidthForPopupMenu = 360;
  SFileName = 'History.dat';
  SDatExt = '.dat';
  SSettingsFile = 'ClipboardView.ini';
  SShortTextFmt = '%s...';
  SGlobalHotKey = 'Win + `'; // MOD_WIN or VK_OEM_3

  SConstraintsSect = 'Constraints';
  SFullHistCountOpt = 'FullHistCount';
  STrayHistCountOpt = 'TrayHistCount';
  SMainWindowSect = 'MainWindow';
  SPopupWindowSect = 'Popup';
  STop = 'Top';
  SLeft = 'Left';
  SHeight = 'Height';
  SWidth = 'Width';
  SWindowState = 'WindowState';

  VK_V = $56;

var
  _SettingsFile: TMemIniFile;

implementation

initialization
  _SettingsFile := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + SSettingsFile);

finalization
  FreeAndNil(_SettingsFile);

end.
